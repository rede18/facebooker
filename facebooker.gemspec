$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "facebooker/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "facebooker"
  s.version     = Facebooker::VERSION
  s.authors     = ["João Vitor Araujo"]
  s.email       = ["joao18araujo@gmail.com"]
  s.homepage    = "https://www.yimobile.com.br"
  s.summary     = "Gem for facebook authentication"
  s.description = "Gem for facebook authentication"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.2.0"
  s.add_dependency "koala"

  s.add_development_dependency "sqlite3"
end
