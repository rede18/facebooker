require_dependency "facebooker/application_controller"

module Facebooker
  class FacebookController < ApplicationController
    def create
      @graph = Koala::Facebook::API.new(facebook_params[:access_token])
      profile = @graph.get_object("me?fields=id,name,email,picture.type(large)")

      begin
        Facebooker.user_class.transaction do

          @user = Facebooker.user_class.find_by(email: profile['email']) if profile['email']

          if @user
              @user.provider = :facebook
              @user.uid = profile['id']
          else
              @user = Facebooker.user_class.find_or_initialize_by(provider: :facebook, uid: profile['id']) do |user|
                  user.role = :pending
              end
              @user.email = profile['email']  || "#{profile['id']}@facebook.com"
              @user.password = Devise.friendly_token[0,20]
          end

          @user.name = profile['name']
          @user.try(:avatar=, profile['picture']['data']['url'])

          @user.try(:facebooker_before_save)

          if @user.save
            @user.try(:facebooker_after_save)
            render json: { access_token: @user.encode_my_jwt, refresh_token: @user.create_my_refresh_token }, status: :created
          else
            render json: @user.errors, status: :unprocessable_entity
          end
        end
      rescue ActiveRecord::RecordNotUnique
        retry
      end
    end

    rescue_from Koala::Facebook::AuthenticationError do |exception|
      respond_to do |format|
        format.json { render json: { 'error': exception.message }, status: :bad_request }
      end
    end

    private

    def facebook_params
      params.require(:facebook).permit(:access_token)
    end
  end
end
