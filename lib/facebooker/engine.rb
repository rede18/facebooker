require 'koala'

module Facebooker
  class Engine < ::Rails::Engine
    isolate_namespace Facebooker
  end
end
