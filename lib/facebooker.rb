require "facebooker/engine"

module Facebooker
  # Your code goes here...
  mattr_accessor :user_class
  mattr_accessor :app_id
  mattr_accessor :app_secret

  def self.user_class
    @@user_class.constantize
  end
end
